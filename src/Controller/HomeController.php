<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    
    public function index(): Response
    {   
        /* Si le cookie n'est pas encore crée (première visite ou lien direct partagé)*/ 
        if (!isset($_COOKIE["darkmode"])){
            setcookie ("darkmode" ,"dark", 0 );
        }
        
        if (!isset($_COOKIE["langue"])){
            setcookie ("langue" ,"fr", 0 );
            return $this->render('fr/home/index.html.twig', [
                'controller_name' => 'HomeController',
            ]);
        }
        
        if($_COOKIE["langue"]=="fr"){
           return $this->render('fr/home/index.html.twig', [
                'controller_name' => 'HomeController',
            ]); 
        }else if($_COOKIE["langue"]=="an"){
            return $this->render('an/home/index.html.twig', [
                'controller_name' => 'HomeController',
            ]); 
        }        
    }

    public function about(): Response
    {
        if (!isset($_COOKIE["darkmode"])){
            setcookie ("darkmode" ,"dark", 0 );
        }
        
        if (!isset($_COOKIE["langue"])){
            setcookie ("langue" ,"fr", 0 );
            return $this->render('fr/about/about.html.twig', [
                'controller_name' => 'HomeController',
            ]);
        }

        if($_COOKIE["langue"]=="fr"){
            return $this->render('fr/about/about.html.twig', [
            'controller_name' => 'HomeController',
            ]);
        }else if($_COOKIE["langue"]=="an"){
            return $this->render('an/about/about.html.twig', [
                'controller_name' => 'HomeController',
            ]);
        }
    }

    public function features(): Response
    {
        if (!isset($_COOKIE["darkmode"])){
            setcookie ("darkmode" ,"dark", 0 );
        }
        
        if (!isset($_COOKIE["langue"])){
            setcookie ("langue" ,"fr", 0 );
            return $this->render('fr/features/features.html.twig', [
                'controller_name' => 'HomeController',
            ]);
        }

        if($_COOKIE["langue"]=="fr"){
            return $this->render('fr/features/features.html.twig', [
            'controller_name' => 'HomeController',
            ]);
        }else if($_COOKIE["langue"]=="an"){
            return $this->render('an/features/features.html.twig', [
                'controller_name' => 'HomeController',
            ]);
        }
    }

    public function websites(): Response
    {
        if (!isset($_COOKIE["darkmode"])){
            setcookie ("darkmode" ,"dark", 0 );
        }
        
        if (!isset($_COOKIE["langue"])){
            setcookie ("langue" ,"fr", 0 );
            return $this->render('fr/websites/websites.html.twig', [
                'controller_name' => 'HomeController',
            ]);
        }
        if($_COOKIE["langue"]=="fr"){
            return $this->render('fr/websites/websites.html.twig', [
            'controller_name' => 'HomeController',
            ]);
        }else if($_COOKIE["langue"]=="an"){
            return $this->render('an/websites/websites.html.twig', [
                'controller_name' => 'HomeController',
            ]);
        }
    }
}
