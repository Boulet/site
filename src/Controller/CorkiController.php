<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CorkiController extends AbstractController
{
    public function entretien(): Response
    {
        return $this->render('fr/websites/garageCorki/entretien.html.twig', [
            'controller_name' => 'CorkiController',
        ]);
    }
    public function depannage(): Response
    {
        return $this->render('fr/websites/garageCorki/depannage.html.twig', [
            'controller_name' => 'CorkiController',
        ]);
    }
    public function contact(): Response
    {
        return $this->render('fr/websites/garageCorki/contact.html.twig', [
            'controller_name' => 'CorkiController',
        ]);
    }
}
