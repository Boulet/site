<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MorganaController extends AbstractController
{
    public function horaires(): Response
    {
        return $this->render('fr/websites/salonMorgana/horaires.html.twig', [
        'controller_name' => 'MorganaController',
        'titre_onglet' => 'Nos horaires'
        ]);
    }
    public function homme(): Response
    {
        return $this->render('fr/websites/salonMorgana/homme.html.twig', [
        'controller_name' => 'MorganaController',
        'titre_onglet' => 'Prendre rendez-vous'
        ]);
    }
    public function femme(): Response
    {
        return $this->render('fr/websites/salonMorgana/femme.html.twig', [
        'controller_name' => 'MorganaController',
        'titre_onglet' => 'Prendre rendez-vous'
        ]);
    }
    public function enfant(): Response
    {
        return $this->render('fr/websites/salonMorgana/enfant.html.twig', [
        'controller_name' => 'MorganaController',
        'titre_onglet' => 'Prendre rendez-vous'
        ]);
    }
    public function contact(): Response
    {
        return $this->render('fr/websites/salonMorgana/contact.html.twig', [
        'controller_name' => 'MorganaController',
        'titre_onglet' => 'Contactez nous'
        ]);
    }
    public function realisations(): Response
    {
        return $this->render('fr/websites/salonMorgana/realisations.html.twig', [
        'controller_name' => 'MorganaController',
        'titre_onglet' => 'Réalisations'
        ]);
    }
    public function date(): Response
    {
        return $this->render('fr/websites/salonMorgana/date.html.twig', [
        'controller_name' => 'MorganaController',
        'titre_onglet' => 'Choix de la date'
        ]);
    }
}
