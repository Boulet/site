<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FeaturesController extends AbstractController
{
    public function postalCode(): Response
    {
        if($_COOKIE["langue"]=="fr"){
            return $this->render('fr/features/postalCode.html.twig', [
            'controller_name' => 'FeaturesController',
            ]);
        }else if($_COOKIE["langue"]=="an"){
            return $this->render('an/features/postalCode.html.twig', [
                'controller_name' => 'FeaturesController',
            ]);
        }
    }

    public function graphic(): Response
    {
        if($_COOKIE["langue"]=="fr"){
            return $this->render('fr/features/graphic.html.twig', [
            'controller_name' => 'FeaturesController',
            ]);
        }else if($_COOKIE["langue"]=="an"){
            return $this->render('fr/features/graphic.html.twig', [
                'controller_name' => 'FeaturesController',
            ]);
        }
    }
}
