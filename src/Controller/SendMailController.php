<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\MailFormType;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class SendMailController extends AbstractController
{
    public function sendMail(Request $request, MailerInterface $mailer): Response
    {
        $form = $this->createForm(MailFormType::class);

        $contact = $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $email = (new TemplatedEmail())
            ->from('test@clement-boulet.fr')
            ->to($contact->get('emailTo')->getData())
            ->subject($contact->get('sujet')->getData())
            ->htmlTemplate('emails/envoieMail.html.twig')
            ->context([
                'mail' => $contact->get('emailTo')->getData(),
                'sujet' => $contact->get('sujet')->getData(),
                'message' => $contact->get('message')->getData()
            ]);
            $mailer->send($email);

            $this->addFlash('message','Votre e-mail à bien été envoyé');
            return $this->redirectToRoute('sendMail');
        }

        return $this->render('fr/features/sendMail.html.twig', [
            'controller_name' => 'SendMailController',
            'form'=>$form->createView()
        ]);
    }
}
