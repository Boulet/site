<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WebsitesController extends AbstractController
{
    public function garageCorki(): Response
    {
        return $this->render('fr/websites/garageCorki/index.html.twig', [
        'controller_name' => 'HomeController',
        ]);
    }

    public function salonMorgana(): Response
    {
        return $this->render('fr/websites/salonMorgana/index.html.twig', [
        'controller_name' => 'HomeController',
        'titre_onglet' => 'Salon de coiffure Morgana'
        ]);
    }
}
