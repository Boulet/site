<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class MailFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('emailTo',EmailType::class,[
                'label'=>'Votre e-mail',
                'attr'=>[
                    'class'=>'form-control'
                ]
            ])    
            ->add('sujet', TextType::class,[
                'label'=>'Objet du mail',
                'attr'=>[
                    'class'=>'form-control'
                ]
            ])
            ->add('message',CKEditorType::class,[
                'config'=>[
                    'iuColor' => '#000000',
                    'toolbar' => 'full'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
