fichierAboutDarkmodeCss="/public/css/about/darkAbout.css";
fichierAboutLightmodeCss="/public/css/about/lightAbout.css";

function majPage(){
    try {
        head.removeChild(document.getElementById("IdDarkmodeAboutCss")); 
    } catch (error) {   }
    if(button.checked){
        addAboutCss(fichierAboutDarkmodeCss);
    }else{
        addAboutCss(fichierAboutLightmodeCss);
    }
}

function addAboutCss(fileName) {//,id
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = fileName;
    link.id = "IdDarkmodeAboutCss";
    head.appendChild(link);
}
