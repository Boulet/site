    
    /*####################
    ##  Initialisation  ##
    ####################*/

//  Les horaires sont déclarés côté client car ce n'est qu'un site vitrine,
//  sinon il faudrait que cela soit fait côté serveur 
let horaires = declarationHoraires();

let divMois = document.getElementById("mois");
let divJours = document.getElementById("jours");
let divHeures = document.getElementById("heures");

const today = new Date();

let premierJourAff;
let dernierJourAff;

affichageInitial();

let liste = declarationJoursAAfficher();

/*  Declaration des horaire du Salon
*   Les lundis et dimanches étant fermés, ils ne seront pas affichés lors du choix de date
*/
function declarationHoraires(){
    var horaires = new Array(6);

    var horairesLundi =  "fermé";
    var horairesMardi = ["13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00"];
    var horairesMercredi = ["09:30","10:00","10:30","11:00","11:30","12:00","12:30", "14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30"];
    var horairesJeudi = ["13:00","13:30","14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30","18:00"];
    var horairesVendredi = ["09:30","10:30","11:30","12:00","12:30", "14:00","15:00","15:30","16:00","16:30","17:00","17:30"];
    var horairesSamedi = ["09:30","11:00","11:30","12:00","12:30", "14:00","14:30","15:00","15:30","16:00","16:30","17:00","17:30"];
    var horairesDimanche = "fermé";
    
    horaires[0]=horairesDimanche;
    horaires[1]=horairesLundi;
    horaires[2]=horairesMardi;
    horaires[3]=horairesMercredi;
    horaires[4]=horairesJeudi;
    horaires[5]=horairesVendredi;
    horaires[6]=horairesSamedi;

return horaires;
}

function affichageInitial(){
    divMois.innerText = MonthNumberToText(today.getMonth()) ;
    affichageJours(today);
    affichageHeures(today);
}

/*
    ####################
    ## Comportements  ##
    ####################
*/

/*  Fonction appelée lors d'un clic sur les bouton suivant ou précédent
*   Paramètre : premier jour affiché dans le tableau (le jour de gauche) de type Date()
*   Sortie : affichage du mois correspondant dans la div "mois"
*/
function majMois(premierJourAff){
    divMois.innerText = MonthNumberToText(premierJourAff.getMonth()) ;
}

/*  fonction appelée lorsque l'on clic sur un jour,
*   elle recherche le jour sur lequel on a cliqué puis affiche les horaires correspondantes
*   Paramètres : un jour et un mois
*   Sorties : la fonction appelle dans un premier temps la fonction de recherche du jour
*       puis dans un second temps elle appelle la fonction d'affichage des heures
*/
function jourOnClick(l_jour,l_utcDate){
    gestionJourActive(l_jour,l_utcDate);
    let leJour = trouverLeJour(l_jour,l_utcDate);
    affichageHeures(leJour);
}

function gestionJourActive(l_jour,l_utcDate){
    let lesActives = document.getElementsByClassName("divActive");
    if(lesActives!=null){
        for (let index = 0; index < lesActives.length; index++) {
            try {
                lesActives[index].classList.remove("divActive");
            }catch (error) {}
            
        }
    }
    
    let divActive = document.getElementById(DayNumberToTexte(l_jour) + l_utcDate);
    divActive.classList.add("divActive");
}

/*  Fonction appelée par jourOnClick
*   
*/
function trouverLeJour(l_jour,utcDate){
    let l_leJour = new Date();
    liste.forEach(s_jour => {
        if(s_jour.getDay()==l_jour && s_jour.getUTCDate()==utcDate){
            l_leJour = s_jour;
        }        
    });
    return l_leJour;
}

/* 
    #####################################
    ##  Affichage des plages horaires  ##
    #####################################
*/

/*  Cette fonction peut être appelée 
*   1 - lors de l'initialisation de départ (afficher les horaires restants aujourd'hui
*   2 - lors d'un clic sur une div "jour" 
*   Paramètre : un jour de type Date() 
*   Sorties : Lors de son fonctionnement, elle appelle régulièrement la fonction ajouterPlageHoraire(horaire) 
*/
function affichageHeures(l_jour){
    divHeures.innerText = "";
    if (l_jour.getDay() != 0 && l_jour.getDay() != 1){
        if(l_jour.getDate() == today.getDate()){
            horaires[l_jour.getDay()].forEach(horaire => {
                let h = today.getHours();
                if (h<10){
                    h = "0" + h;
                }
                let heure = h + ":" + today.getMinutes();
                if(horaire > heure){
                    ajouterPlageHoraire(horaire);
                }
            });
        }else{
            horaires[l_jour.getDay()].forEach(horaire => {
                ajouterPlageHoraire(horaire);
            });
        }
    }
}

/*  Cette fonction est appelée par affichageHeures(jour)
*   Paramètre : prend en paramètre un horaire au format xx:xx 
*   Sortie : Affichage d'un paragraphe contenant l'horaire dans une division
*/
function ajouterPlageHoraire(horaire){
    var plageHoraire = document.createElement("div");
    plageHoraire.classList.add("heure");
    plageHoraire.classList.add("col-lg-2");
    plageHoraire.classList.add("col-5");
    plageHoraire.classList.add("text-center");
    plageHoraire.classList.add("align-middle");
    plageHoraire.innerHTML= "<p class=\"align-middle\">" + horaire + "</p>";  
    divHeures.appendChild(plageHoraire);

}

/*
    ###########################
    ##  Affichage des jours  ##
    ###########################
*/

/*  Fonction appelée lors de l'affichage des jours
*   Paramètre : un jour de type Date()
*   Sortie : un paragraphe contenant le jour et la date au format "abc. xx" dans une division "jour"
*/
function ajouterJour(l_jour){
    
    let journee = document.createElement("div");
    journee.classList.add("jour");
    journee.classList.add("col-lg-2");
    journee.classList.add("col-12");
    journee.classList.add("text-center");
    journee.classList.add("align-middle");
    journee.id=DayNumberToTexte(l_jour.getDay()) + l_jour.getUTCDate();
    journee.addEventListener('click',event=>{
        jourOnClick(l_jour.getDay(), l_jour.getUTCDate() );
        //jourOnClick(l_jour.getDay(), l_jour.getMonth() );
    })
    journee.innerHTML= "<p class=\"align-middle\">" + DayNumberToTexte(l_jour.getDay()) + " " + l_jour.getUTCDate() + "</p>";  
    divJours.appendChild(journee);
}

/*  Les deux fonctions si dessous sont a peu de chose prêt identiques
*   la premiere affiche le bouton "suivant" et est appelée lors de l'initialisation
*   La seconde affiche le bouton "precedent" et est appelée lorsqu'il y a déjà eu un clic sur le bouton next 
*   Sortie : un bouton dans la div "jours"
*/
function ajouterBtnNext(){
    let btn = document.createElement("button");
    btn.id="suivant";
    btn.addEventListener('click',event=>{      
        affichageJours(dernierJourAff);
        majMois(premierJourAff);
    })
    btn.innerHTML= ">";  
    divJours.appendChild(btn);
}

function ajouterBtnPre(){
    let btn = document.createElement("button");
    btn.id="precedent";
    btn.addEventListener('click',event=>{
        affichageInitial();
        majMois(premierJourAff);
    })
    btn.innerHTML= "<";  
    divJours.appendChild(btn);
}

/*  Appelée pour afficher les jours
*   Paramètre : un jour au format Date()
*   Sorties : Appel des fonctions d'ajout des boutons suivant / précedents selon le context,
        appel de la fonction ajoutant les jours dans la divisions "jours"
        cette fonction alimente aussi "premierJourAff" et "dernierJourAff"
*   Commentaire : Cette fonction devrait sûrement être découpée en plusieurs petites fonctions car elle fait trop de choses
*/
function affichageJours(l_jour){
    divJours.innerHTML="";
    premierJourAff=new Date(l_jour);
    if(premierJourAff.getUTCDate() != today.getUTCDate()){
        ajouterBtnPre();
    }
    let stop = false; 
    let jourAffiches = 0;
    let decalage = 0;
    while ( (jourAffiches < 5) && (stop == false) ){
        let s_jour = new Date(l_jour);
        s_jour.setDate(s_jour.getDate() + decalage);
        if ((s_jour.getDay() != 0  && s_jour.getDay() != 1) ){
            if((s_jour.getMonth() == l_jour.getMonth())){
                ajouterJour(s_jour);
                jourAffiches++; 
            }else{
            stop = true;
            }  
        } 
        decalage++;
        if(decalage==5){
            stop=true;
        }
    }
    if(premierJourAff.getUTCDate() == today.getUTCDate()){
        ajouterBtnNext();
    }
    dernierJourAff = new Date(l_jour);
    dernierJourAff.setDate(dernierJourAff.getDate()+decalage+1);
}

/*  Cette fonction est utilisée lorsque l'on cherche le jour sur lequel
*   l'utilisateur à cliqué
*   Sortie : initalisation du tableau des jours que l'on peut afficher
*/
function declarationJoursAAfficher(){
    let tableau = new Array;
    for (let index = 0; index < 14; index++) {
        let l_jour = new Date();
        tableau[index]=l_jour;
        tableau[index].setDate(tableau[index].getDate()+index);
    }
    return tableau;
}

/* 
    ###################
    ##  Traductions  ##
    ###################
*/

/*  Etant donné qu'en JavaScript lorsque l'on récupère le mois d'une date nous récupérons soit un entier soit le mois en anglais,
*   il faut le transformer en texte français
*   Paramètre : un mois de type Entier
*   Sortie : du texte
*/
function MonthNumberToText(number){
    let mois_local;
    switch(number){
        case 0:
            mois_local='janvier';
        break;

        case 1:
            mois_local='Février';
        break;

        case 2:
            mois_local="Mars";
        break;

        case 3:
            mois_local="Avril";
        break;

        case 4:
            mois_local='Mai';
        break;

        case 5:
            mois_local='Juin';
        break;
        
        case 6:
            mois_local='Juillet';
        break;

        case 7:
            mois_local='Août';
        break;

        case 8:
            mois_local='Septembre';
        break;

        case 9:
            mois_local='Octobre';
        break;

        case 10:
            mois_local='Novembre';
        break;

        case 11:
            mois_local='Décembre';
        break;        
    }
    return mois_local;
}
/*  Même principe que MonthNumberToText, il faut traduire l'entier renvoyé par getDay() 
*   Paramètre : un jour de type Entier
*   Sortie : du texte
*/
function DayNumberToTexte(jourBrut){
    let jour_local;
    switch(jourBrut){
        case 0:
            jour_local="Dim.";
        break;

        case 1:
            jour_local='Lun.';
        break;

        case 2:
            jour_local='Mar.';
        break;

        case 3:
            jour_local="Mer.";
        break;

        case 4:
            jour_local="Jeu.";
        break;

        case 5:
            jour_local="Ven.";
        break;

        case 6:
            jour_local="Sam.";
        break;        
    }
    return jour_local;
}