var lienHoraires = document.getElementById("horaires");
var lienRDV = document.getElementById("prendreRDV");
var lienContact = document.getElementById("contact");
var lienRealisations = document.getElementById("realisations");
var titre = document.getElementById("titreOnglet");

if(titre.innerText=="Nos horaires"){
    lienHoraires.classList.add("active");
}
else if(titre.innerText=="Prendre rendez-vous" || titre.innerText=="Choix de la date"){
    lienRDV.classList.add("active");
}else if(titre.innerText=='Réalisations'){
    lienRealisations.classList.add("active");
}
else if(titre.innerText=="Contactez nous"){
    lienContact.classList.add("active");
}
