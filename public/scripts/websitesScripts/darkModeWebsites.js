fichierWebsitesDarkmodeCss="/public/css/websites/darkWebsites.css";
fichierWebsitesLightmodeCss="/public/css/websites/lightWebsites.css";

var websitesCards = new Array(1);
websitesCards[0] = document.getElementById("corkiCard");
websitesCards[1] = document.getElementById("morganaCard");

function majPage(){
    try {
        head.removeChild(document.getElementById("IdDarkmodeWebsitesCss")); 
    } catch (error) {   }
    if(button.checked){
        addWebsitesCss(fichierWebsitesDarkmodeCss);
    }else{
        addWebsitesCss(fichierWebsitesLightmodeCss);
    }
    removeCardClass();
    addCardClass();
}

function addWebsitesCss(fileName) {//,id
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = fileName;
    link.id = "IdDarkmodeWebsitesCss";
    head.appendChild(link);
}
function removeCardClass(){
    if(getCookie("darkmode")=="dark"){  
        websitesCards.forEach(websiteCard => {
            try{
                websiteCard.classList.remove("bg-light");
                websiteCard.classList.remove("text-black");
            }catch (error){}
        });
    }else{       
        websitesCards.forEach(websiteCard => {
            try{
                websiteCard.classList.remove("bg-secondary");
                websiteCard.classList.remove("text-white");
            }catch (error){}
        });        
    }       
}

function addCardClass(){
    if(getCookie("darkmode")=="dark"){
        websitesCards.forEach(websiteCard => {
            try{
                websiteCard.classList.add("bg-secondary");
                websiteCard.classList.add("text-white");
            }catch (error){}
        });
    }else{
        websitesCards.forEach(websiteCard => {
            try{
                websiteCard.classList.add("bg-light");
                websiteCard.classList.add("text-black");
            }catch (error){}
        });
    }   
}

