var button=document.getElementById("flexSwitchCheckChecked");
var choixFR = document.getElementById("langueFR");
var choixAN = document.getElementById("langueAN");


choixFR.addEventListener('click',function(e){
    setCookie("langue","fr");
    if (button.checked){
        choixFR.classList.add("monActiveDark");
        choixAN.classList.add("monInactiveDark");

        choixFR.classList.remove("monInactiveDark");
        choixAN.classList.remove("monActiveDark");
    }else{
        choixFR.classList.add("monActiveLight");
        choixAN.classList.add("monInactiveLight");
        
        choixFR.classList.remove("monInactiveLight");
        choixAN.classList.remove("monActiveLight");
    }
});

choixAN.addEventListener('click',function(e){
    setCookie("langue","an");
    if (button.checked){
        choixAN.classList.add("monActiveDark");
        choixFR.classList.add("monInactiveDark");

        choixAN.classList.remove("monInactiveDark");
        choixFR.classList.remove("monActiveDark");

    }else{
        choixAN.classList.add("monActiveLight");
        choixFR.classList.add("monInactiveLight");
        
        choixAN.classList.remove("monInactiveLight");
        choixFR.classList.remove("monActiveLight");
        
    }
});

function setCookie(name,value) {
    document.cookie = name + "=" + value;
  }
