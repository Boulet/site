fichierDarkmodeCss="/public/css/darkBase.css";
fichierLightmodeCss="/public/css/lightBase.css";
    
var head = document.head;
var button=document.getElementById("flexSwitchCheckChecked");
var maNavbar = document.getElementById("myNavbar");
var maCheckbox = document.getElementsByName("maCheckbox");
var monDropDown = document.getElementById("monDropDown");
var choixFr = document.getElementById("langueFR");
var choixAn = document.getElementById("langueAN");
var Mavariable;

button.addEventListener('click',function(e){
  maj();
});

function updateDropDown(darkmode){
  if(darkmode=="dark"){
    if(getCookie("langue")=='fr'){
      choixFr.classList.remove("monActiveLight");
      choixFr.classList.add("monActiveDark");

      choixAn.classList.remove("monInactiveLight");
      choixAn.classList.add("monInactiveDark");

    }else if (getCookie("langue")=="an"){
      choixAn.classList.remove("monActiveLight");
      choixAn.classList.add("monActiveDark");

      choixFr.classList.remove("monInactiveLight");
      choixFr.classList.add("monInactiveDark");
    }
  }else if (darkmode=="light"){
    if(getCookie("langue")=="fr"){
      choixFr.classList.remove("monActiveDark");
      choixFr.classList.add("monActiveLight");

      choixAn.classList.remove("monInactiveDark");
      choixAn.classList.add("monInactiveLight");

    }else if (getCookie("langue")=="an"){
      choixAn.classList.remove("monActiveDark");
      choixAn.classList.add("monActiveLight");

      choixFr.classList.remove("monInactiveDark");
      choixFr.classList.add("monInactiveLight");
    }
  }
}
//Quand la page à finit de charger 
window.addEventListener("DOMContentLoaded", (event) => {
  let darkmode = getCookie("darkmode");
  let langue = getCookie("langue");

  addDarkmodeCss(darkmode);
  majEtatSwitch(darkmode); 
  maj();
  
  removeLanguageClass();
  addLanguageActiveClass(langue,darkmode);  
  majPage();
  
});

function removeLanguageClass(){
  choixFr.classList.remove("monInactiveDark");
  choixFr.classList.remove("monActiveDark");
  choixFr.classList.remove("monInactiveLight");
  choixFr.classList.remove("monActiveLight");

  choixAn.classList.remove("monInactiveDark");
  choixAn.classList.remove("monActiveDark");
  choixAn.classList.remove("monInactiveLight");
  choixAn.classList.remove("monActiveLight");
}

function addCss(fileName) {//,id
  var link = document.createElement("link");
  link.type = "text/css";
  link.rel = "stylesheet";
  link.href = fileName;
  link.id = "IdDarkmodeCss";
  head.appendChild(link);
}
  
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(name,value) {
  document.cookie = name + "=" + value;
}

function addDarkmodeCss(darkmode){
  if (darkmode == "dark"){
    addCss(fichierDarkmodeCss);
  }else if(darkmode == "light"){
    addCss(fichierLightmodeCss);
  }
}

function addLanguageActiveClass(langue, darkmode){
  if (langue == "fr" && darkmode == "dark"){
    choixFr.classList.add("monActiveDark");
    choixAn.classList.add("monInactiveDark");      
  }else if (langue == "fr" && darkmode == "light"){
    choixFr.classList.add("monActiveLight");
    choixAn.classList.add("monInactiveLight");
  }else if (langue == "an" && darkmode == "dark"){
    choixAn.classList.add("monActiveDark");
    choixFr.classList.add("monInactiveDark");
  }else if (langue == "an" && darkmode == "light"){
    choixAn.classList.add("monActiveLight");
    choixFr.classList.add("monInactiveLight");
  }
}

function majEtatSwitch(darkmode){
  if (darkmode == "dark"){
    button.checked = true;
  }else if(darkmode == "light"){
    button.checked = false;
  }
}
 
function maj(){
  if (button.checked){
    //Fichiers Css à enlever
    try{
      head.removeChild(document.getElementById("IdDarkmodeCss"));
    }catch(error){}
    

    //Fichiers Css à ajouter
    addCss(fichierDarkmodeCss);
    setCookie("darkmode","dark"); // Maj du cookie

    //Class Bootstrap à enlever
    maNavbar.classList.remove("navbar-dark");
    maNavbar.classList.remove("bg-dark");
    button.classList.remove("bg-light");
    monDropDown.classList.remove("bg-dark");

    //Class Bootstrap à ajouter
    maNavbar.classList.add("navbar-light");
    maNavbar.classList.add("bg-light");
    button.classList.add("bg-dark");
    monDropDown.classList.add("bg-light")

    //Simples changements 
    document.getElementById("monDivider").style.borderColor = "black";
    //Appel des fonctions 
    updateDropDown("dark");

  }else{
    //Fichiers Css à enlever
    head.removeChild(document.getElementById("IdDarkmodeCss"));

    //Fichiers Css à ajouter
    addCss(fichierLightmodeCss);
    setCookie("darkmode","light"); //Maj du cookie

    //Class Bootstrap à enlever
    maNavbar.classList.remove("navbar-light");
    maNavbar.classList.remove("bg-light");
    button.classList.remove("bg-dark");
    monDropDown.classList.remove("bg-light");

    //Class Bootstrap à ajouter
    maNavbar.classList.add("navbar-dark");
    maNavbar.classList.add("bg-dark");
    button.classList.add("bg-light");
    monDropDown.classList.add("bg-dark");

    //Simples changements 
    document.getElementById("monDivider").style.borderColor = "white";

    //Appel des fonctions 
    updateDropDown("light");
  }
  majPage();
}