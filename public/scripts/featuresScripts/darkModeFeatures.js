fichierFeaturesDarkmodeCss="/public/css/features/darkFeatures.css";
fichierFeaturesLightmodeCss="/public/css/features/lightFeatures.css";

var featuresCards = new Array(2);
featuresCards[0] = document.getElementById("APICard");
featuresCards[1] = document.getElementById("sendEmailCard");
featuresCards[2] = document.getElementById("graphicCard");

function majPage(){
    try {
        head.removeChild(document.getElementById("IdDarkmodeFeaturesCss")); 
    } catch (error) {   }
    if(button.checked){
        addFeaturesCss(fichierFeaturesDarkmodeCss);
    }else{
        addFeaturesCss(fichierFeaturesLightmodeCss);
    }
    removeCardClass();
    addCardClass();
}

function addFeaturesCss(fileName) {//,id
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = fileName;
    link.id = "IdDarkmodeFeaturesCss";
    head.appendChild(link);
}

function removeCardClass(){
    if(getCookie("darkmode")=="dark"){  
        featuresCards.forEach(featuresCard => {
            try{
                featuresCard.classList.remove("bg-light");
                featuresCard.classList.remove("text-black");
            }catch (error){}
        });
    }else{       
        featuresCards.forEach(featuresCard => {
            try{
                featuresCard.classList.remove("bg-secondary");
                featuresCard.classList.remove("text-white");
            }catch (error){}
        });        
    }       
}

function addCardClass(){
    if(getCookie("darkmode")=="dark"){
        featuresCards.forEach(featuresCard => {
            try{
                featuresCard.classList.add("bg-secondary");
                featuresCard.classList.add("text-white");
            }catch (error){}
        });
    }else{
        featuresCards.forEach(featuresCard => {
            try{
                featuresCard.classList.add("bg-light");
                featuresCard.classList.add("text-black");
            }catch (error){}
        });
    }   
}