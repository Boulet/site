var divResultats = document.getElementById ('resultats');

$("#rechercher").click(function(){
    search();
});
    
$(document).on('keypress',function(e) {
    if(e.which == 13) {
        search();
    }
});
    
function search(){
    let codePostal=(($("#entree").val())).toString();
    let url = "https://apicarto.ign.fr/api/codes-postaux/communes/"+codePostal;
    $.getJSON(url,function(data){
        console.log(data);
        /* Retrait des anciens résultats s'il y en avait */
        delPreviousResults();
        /* Ajout des nouveaux resultat de la recherche dans un tableau */
         addResults(data);
    }).fail(function(jqXHR){
        displayError(jqXHR.status);
    });
}

function displayError(error){
    delPreviousResults();
    var alertDiv = document.createElement("div");
    alertDiv.classList.add("alert");
    alertDiv.classList.add("alert-danger");
    if (error == 400){
        /* Saisie invalide */
        if(getCookie("langue")=="fr"){
            alertDiv.innerHTML="Saisie invalide, veuillez Entrer 5 chiffres sans espaces.";
        }else if(getCookie("langue")=="an") {
            alertDiv.innerHTML="Invalid data.";
        }
    }else if (error == 404){
        /* Code postal non attribué */
        if ((($("#entree").val())).toString()==""){
            if(getCookie("langue")=="fr"){
                alertDiv.innerHTML="Vous n'avez pas entré de code postal.";
            }else if(getCookie("langue")=="an") {
                alertDiv.innerHTML="Emptry entry.";
            }   
        }else {
            if(getCookie("langue")=="fr"){
                alertDiv.innerHTML="Ce code postal n'est pas attribué.";
            }else if(getCookie("langue")=="an") {
                alertDiv.innerHTML="This zip code is not assigned.";
            }
        }      
    }
    divResultats.appendChild(alertDiv);
}

function delPreviousResults(){
    divResultats.innerHTML="";
}

function addResults(data){
    var table = document.createElement("table");
    table.classList.add("table");
    if(getCookie("darkmode")=="dark"){
        table.classList.add("table-dark");
    }else{
        table.classList.add("table-secondary");
    }    
    data.forEach(commune => {
        var row = table.insertRow();
        var cell = row.insertCell();
        cell.innerHTML = commune.nomCommune;
    });   
    divResultats.appendChild(table);
}