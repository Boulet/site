let donnees = [
  {"date": "2020-01-01", "valeur": 49}, {"date": "2020-02-01", "valeur": 51}, {"date": "2020-03-01", "valeur": 52},
  {"date": "2020-04-01", "valeur": 53}, {"date": "2020-05-01", "valeur": 54}, {"date": "2020-06-01", "valeur": 53},
  {"date": "2020-07-01", "valeur": 52.5}, {"date": "2020-08-01", "valeur": 54.5}, {"date": "2020-09-01", "valeur": 53},
  {"date": "2020-10-01", "valeur": 54}, {"date": "2020-11-01", "valeur": 51.2}, {"date": "2020-12-01", "valeur": 53.5}
];

var vlSpec = {
    $schema: 'https://vega.github.io/schema/vega-lite/v5.json',
    "width": "container",
    "data": {
        "values": donnees
      },
      "mark": "line",
      "encoding": {
        "x": {"field": "date",
              "timeUnit": "month",
              "axis": {"labelAlign": "left", "labelExpr": "datum.label[0]"},
              "title":"Année 2019"},
        "y": {"field": "valeur", "type": "quantitative","title":"Chiffre d'affaire (en milliers d'€)","scale": {"zero": false}}
      }
};

vegaEmbed('#vis', vlSpec);

var inputMois = new Array(12);
inputMois[0] = document.getElementById("janvierInput");
inputMois[1] = document.getElementById("fevrierInput");
inputMois[2] = document.getElementById("marsInput");
inputMois[3] = document.getElementById("avrilInput");
inputMois[4] = document.getElementById("maiInput");
inputMois[5] = document.getElementById("juinInput");
inputMois[6] = document.getElementById("juilletInput");
inputMois[7] = document.getElementById("aoutInput");
inputMois[8] = document.getElementById("septembreInput");
inputMois[9] = document.getElementById("octobreInput");
inputMois[10] = document.getElementById("novembreInput");
inputMois[11] = document.getElementById("decembreInput");

var buttonModifier = document.getElementById("btnModifier");

buttonModifier.addEventListener('click',function(e){
  getModifications();
  vegaEmbed('#vis', vlSpec);
});

function getModifications(){
  for (let index = 0; index < inputMois.length; index++) {
    if(inputMois[index].value != ""){
      donnees[index].valeur = inputMois[index].value;
    }
  }
}

