fichierFeaturesDarkmodeCss="/public/css/features/darkFeatures.css";
fichierFeaturesLightmodeCss="/public/css/features/lightFeatures.css";

function majPage(){
    try {
        head.removeChild(document.getElementById("IdDarkmodeFeaturesCss")); 
    } catch (error) {   }
    if(button.checked){
        addFeaturesCss(fichierFeaturesDarkmodeCss);
    }else{
        addFeaturesCss(fichierFeaturesLightmodeCss);
    }
}

function addFeaturesCss(fileName) {//,id
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = fileName;
    link.id = "IdDarkmodeFeaturesCss";
    head.appendChild(link);
}


