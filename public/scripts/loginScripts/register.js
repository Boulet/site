var inputs = new Array(3);
inputs[0] = document.getElementById("registration_form_email");
inputs[1] = document.getElementById("registration_form_plainPassword_first");
inputs[2] = document.getElementById("registration_form_plainPassword_second");
var divResultats = document.getElementById ('resultats');

window.addEventListener("DOMContentLoaded", (event) => {
    addInputClass();
    displayErrors();
});

function addInputClass(){
    inputs.forEach(input => {
        try{
            input.classList.add("form-control");
        }catch (erreurs){}
    });  
}
function displayErrors(){
    var erreurs = document.getElementById("gestionErreurs").getElementsByTagName("li");
    console.log(erreurs.length);
    let time = 3000;
    for (let erreur of erreurs) {
        setTimeout(() => {traductionErreurs(erreur.innerText);},time-3000);
        setTimeout(() => {delError();},time);
        time += 4000;
    }    
}
function traductionErreurs(text){
    if (text === "There is already an account with this email"){
        text = "Cette adresse email est déjà utilisée";
    }
    addError(text);
}
function addError(text){
    var div = document.createElement("div");
    div.classList.add("alert");
    div.classList.add("alert-danger");

    div.innerText = text;   

    divResultats.appendChild(div);
}
function delError(){
    divResultats.innerHTML="";
}
