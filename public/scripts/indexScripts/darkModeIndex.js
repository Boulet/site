fichierIndexDarkmodeCss="/public/css/index/darkIndex.css";
fichierIndexLightmodeCss="/public/css/index/lightIndex.css";

function majPage(){
    try {
        head.removeChild(document.getElementById("IdDarkmodeIndexCss"));
    } catch (error) {   }
    if(button.checked){
        addIndexCss(fichierIndexDarkmodeCss);
    }else{
        addIndexCss(fichierIndexLightmodeCss);
    }
}

function addIndexCss(fileName) {//,id
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = fileName;
    link.id = "IdDarkmodeIndexCss";
    head.appendChild(link);
  }